//
//  ContentView.swift
//  ToDoApp
//
import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            TodosList()
                .tabItem {
                    Image(systemName: "pencil.and.ellipsis.rectangle")
                    Text("Todos")
                        .font(.title)
                }
            CompletedTodos()
                .tabItem {
                    Image(systemName: "checkmark.circle.fill")
                    Text("Completed")
                        .font(.title)
            }
        }
        .font(.headline)
    }
}
