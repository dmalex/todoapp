//
//  CompletedTodos.swift
//  ToDoApp
//
import SwiftUI

struct CompletedTodos: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: Todo.entity(), sortDescriptors: [], predicate: NSPredicate(format: "isCompleted == TRUE")) var completedTodos: FetchedResults<Todo>
    
    
    var body: some View {
        List {
            ForEach(self.completedTodos, id:\.id){ todo in
                    CompletedTodoRow(todo: todo)
            }
            .edgesIgnoringSafeArea(.top)
        }
    }
}
