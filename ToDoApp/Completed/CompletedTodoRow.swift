//
//  CompletedTodoRow.swift
//  ToDoApp
//
import SwiftUI

struct CompletedTodoRow: View {
    let todo: Todo
    var body: some View {
        HStack {
            Image(systemName: "checkmark.seal.fill")
                .padding()
            Text(todo.wrappedTask)
                .lineLimit(1)
                .padding()
        }
    }
}
