//
//  TaskPriority.swift
//  ToDoApp
//
import Foundation

enum TaskPriority: Int16, CaseIterable, Hashable
{
    case hightest = 1
    case high
    case low
    case lowest
    
    var name: String {
        switch self {
        case .hightest:
            return "!"
        case .high:
            return "!!"
        case .low:
            return "!!!"
        case .lowest:
            return "!!!!"
        }
    }
}
