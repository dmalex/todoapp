//
//  DateExtenstions.swift
//  ToDoApp
//
import Foundation

extension Date {
    var ShortDate: String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
}
